using JLD

function save_run(sol, pg, pp, pn, run_name,fp_save;verbose=false)
    @unpack sgrid, ht0, tspan, tout = pg

    zs_=[]
    zb_=[]
    width_array= []
    for j = 1:length(pg.tout)
        push!(zs_, zs.(pg.sgrid,Ref(pg.tout[j]),Ref(pg)))
        push!(zb_, zb.(pg.sgrid,Ref(pg.tout[j]),Ref(pg)))
        push!(width_array, width.(pg.sgrid,Ref(pg.tout[j]),Ref(pg)))
    end

    width_array = hcat(width_array...)
    zs_ = hcat(zs_...)
    zb_ = hcat(zb_...)

    hts, dht_dts, sed_source, Qws, Qws_sm, Qbs, Qbes = post_proc(sol,pg,pp,pn)


    #V_source_rate = extract_sed_source(sol,pg,pp,pn)

 #   hts = hcat(hts...)
  #  verbose && (println("post-proc end"); toc())

    ###########################
    ### save test variables ###
    ###########################

    JLD.save("$(fp_save)$(run_name).jld",
             "z_surf",    zs_, #geometry
             "z_bed",    zb_,
             "width", width_array,
             "tout",       pg.tout, #parameters
             "sgrid",      pg.sgrid,
             "ht0",        pg.ht0,
             "fsl",        pg.fsl,
             "Dm",         pp.Dm,
             "lr",         pp.lr,
             "fi",         pp.fi,
             "DT",         pp.DT,
             "DAMP",       pp.DAMP,
             "DDAMP",      pp.DDAMP,

             "Qws",   Qws, #outputs
             #"dphi_dx", dphi_dx,
             "Qws_sm",Qws_sm,
             "Qbes",  Qbes,
             "hts",   hts,
             "sed_source", sed_source,
             "Qbs",   Qbs
             )
    return  hts, dht_dts,sed_source, Qws, Qws_sm, Qbs, Qbes

end

function extract_sed_source(sol,pg,pp,pn)
    dt = (pg.tout[end]-pg.tout[1])/(length(pg.tout))
    smid, ds = get_smid_ds(pg.sgrid)
    V_source_rate=zeros(length(sol.t))

    for (i,t) in enumerate(pg.tout)
        ht = sol(t)
        V_source_rate[i] = sum(effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), ht).*width.(smid,t,Ref(pg)).*ds)
    end

    return V_source_rate
end



"""
    hts, dht_dts, sed_source, Qws, Qws_sm, Qbs, Qbes = post_proc(sol,pg,pp,pn)

Post_proc to process results of the objfun called in sol  = run_model(pg, pp, pn).

this version is heavy on the output. However, these can be commented out for increased speed.
"""
function post_proc(sol,pg,pp,pn; tout=pg.tout)
    @unpack sgrid, ht0, tspan = pg

    smid, ds = get_smid_ds(sgrid)


    # post-proc
    hts     =  sol(tout)
    Qws     =  zeros(length(sgrid), length(tout))
    Qws_sm  =  zeros(length(sgrid), length(tout))
    dphi_dx =  zeros(length(sgrid), length(tout))
    Qbs     =  zeros(length(sgrid), length(tout))
    Qbes    =  zeros(length(sgrid), length(tout))
    dht_dts =  zeros(length(sgrid)-1, length(tout))
    sed_source =  zeros(length(sgrid)-1, length(tout))
    Qws_src =  zeros(length(sgrid)-1, length(tout))

    for i in eachindex(tout)
        t = tout[i]
        Qws[:,i] = Q_w(sgrid, t, pg, pp)
        Qws_sm[:,i] = Q_w_sm(sgrid, t, pg, pp)
        dht_dts[:,i],Qbs[:,i],Qbes[:,i], Qws[:,i] = dht_dt_fn(hts[i], sgrid, t, pg, pp, pn)
        sed_source[:,i] = effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), hts[i]).* width.(smid, Ref(t), Ref(pg)).*ds
        
    end

    hts = hcat(hts...)

    return hts, dht_dts, sed_source, Qws, Qws_sm, Qws_src, Qbs, Qbes
end

"""
    time_step_comp(sol,data,time_period, time_span)

    function to returns comparison of model vs data over a certain time period
    note that data must be have same time structure
"""
function time_step_comp(model,data,time_period, time_span)

    tstep_new=(time_span[1]:time_period:time_span[end])

    dt = time_span[2]-time_span[1]
    smoothing_indi = Int(round(time_period/dt))

    new_dataset  = VAWTools.boxcar(data,smoothing_indi)
    new_modelset = VAWTools.boxcar(model,smoothing_indi)

    itp_new_data = Interpolations.interpolate((time_span,),new_dataset, Interpolations.Gridded(Constant()))
    itp_new_model = Interpolations.interpolate((time_span,),new_modelset, Interpolations.Gridded(Constant()))

    data_out  = itp_new_data(tstep_new)
    model_out = itp_new_model(tstep_new)

    # spl_model = Spline1D(time_span, model,k=5)
    # spl_data = Spline1D(time_span, data,k=5)

    # model_vols = zeros(length(tstep_new))
    # data_vols  = zeros(length(tstep_new))

    # for i = 1:length(tstep_new)-1
    #     model_vols[i] = integrate(spl_model, tstep_new[i], tstep_new[i]+1)#*(tstep_new[2]-tstep_new[1])

    #     data_vols[i]= integrate(spl_data, tstep_new[i], tstep_new[i]+1)#*(tstep_new[2]-tstep_new[1])
    # end

    return model_out, data_out
end

"""
    abs_error(model,data)
"""
function abs_error(model,data)

    out = sum(abs.(model - data))

end

"""
abs_error(model,data)
"""
function root_error(model,data)
    out = sum((model - data).^2)
end

"""
    mass_con_check(sol, pg, pp, pn)

Checks mass conservation by comparing proglacial sediment discharge and till layer change.
"""
function mass_con_check(sol, pg, pp, pn; tspan=(pg.tout[1], pg.tout[end]), dt=0.1*day, verbose=true)
    @unpack sgrid = pg

    tout = tspan[1]:dt:tspan[2]

    smid, ds = get_smid_ds(sgrid)

    # glacier width (assumes fixed topo)
    w = width.(smid, Ref((tspan[2]-tspan[1])*0.5), Ref(pg)
)
    # volume change in till layer
    Δhts = sol(tspan[2]) - sol(tspan[1])
    ΔV = sum(Δhts.*w.*ds)


    # integrate in time:
    V_source = 0.0
    V_progl = 0.0
    for (i,t) in enumerate(tout)

        ht = sol(t)
        _, Qb, _ = SUGSET.dht_dt_fn(ht, sgrid, t, pg, pp, pn)
        V_progl += Qb[1]*dt
        V_source += sum(effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), ht).* width.(smid, Ref(t), Ref(pg)).*ds*dt)
    end

    mass_con = 1-V_progl/(ΔV - V_source)

    verbose && println("\n
                        \t  !!! MASS CON: !!!
                        till model ΔV+source = $(round(ΔV - V_source; digits= 6)) (m³)
                        Source               =  $(round(V_source; digits = 6)) (m³)
                        Q_s V                = $(round(V_progl;digits = 6)) (m³)
                        fractional diff      = $(round(mass_con;digits = 3))"
                       )
    return mass_con, V_progl, ΔV - V_source, V_source
end

"""
    Qbs_post_proc(sol,pg,pp,pn)

may have some issues!!!! Use with caution
"""
function Qbs_post_proc(sol,pg,pp,pn)
    println("Need further comparison with post_proc()... Use with caution!!!!!")
    smid = zeros(length(pg.sgrid)-1)
    for i =length(pg.sgrid)-1:-1:1
        smid[i] = (pg.sgrid[i+1] + pg.sgrid[i])/2 #non stagard grid
    end

    w = zeros(length(smid))

#    for i =1:length(smid)
 #      w[i]= width(smid[i],0,pg)
 #   end

    cell_length= pg.sgrid[2]-pg.sgrid[1]

    Qbs  =  zeros( length(pg.tout))

    for i in eachindex(pg.tout)
        t = pg.tout[i]
        w= width.(smid,Ref(t),Ref(pg))
        dht_dt = sol(t, Val{1})
        mt = SUGSET.effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), sol(t))
        Qbs[i] = sum(w .* cell_length.*(mt .- dht_dt))
    end

    return Qbs
end
