using Roots
using Interpolations, LinearAlgebra
using RandomNumbers

##############################
### SHMIP - Valley glacier ###
##############################

"Defines a valley (Bench) glacier"
@with_kw struct Valley<:Glacier @deftype Float64
    # The spatial domain:
    domain::Tuple{Float64, Float64}=(0.0, 6000.0)
    # Source terms
    mw = 0 # water source term
    Mw = 0 # water source if stead
    mb = 0 #.03/(86400*365) # sediment source term meters per year...
    Mb0 = 0
    # the finite volume grid (the boundaries not cell-centers):
    sgrid::LinRange{Float64}=range(domain[1],stop=domain[2],length=300)
    ds = abs(sgrid[2] - sgrid[1])

    # Time domain
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}=(0.0, 1.0)
    tout::Vector{Float64}=tspan[1]:0.25*day:tspan[2] # output times
    #@assert tout[1]>=tspan[1]
    #@assert tout[2]<=tspan[2]

    # IC & BC
    ht0::Vector{Float64}=sgrid[1:end-1]*0.0 .+ .1 # initial till layer thickness
    Q_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top

    # source averaging
    source_average_time=day*2.5
    source_quantile = 0.75

    #till
    till_growth_lim = 0.75 # maximium amount of till, above which till stops being produced.
    fsl = 1.0# fraction of basal sliding
    till_lim = 1.0
    stoch = false # implement stochastic transport

    # misc p
    # Topo
    para = 0.05
    spin::Bool = false
    spin_fac = 1
    # misc parameters
    float_fracˣ = 0.99 # 1<=>overburden pressure, 0<=>atmospheric
    @assert 0<=float_fracˣ<1

    gamma= -0.0008./year # mass balance gradient per second
    B_0_term = 0
end

"""
    source_water(s, t, pg::Valley, pp::Phys)

Water source term in m/s.
"""
source_water(s,t,pg::Valley,pp) =  source_water_ddm(s,t,pg,pp)


### ... and its geometry ###


function f(s,t,pg)
    sg = pg.sgrid[end]
    (zs(sg,t,pg) - pg.para.*sg)/sg^2 * s^2 + pg.para*s
end
h(s,t, pg) = (-4.5*s/pg.sgrid[end] + 5) * (zs(s,t,pg)-f(s,t, pg)) / (zs(s,t,pg)-f(s,t, pg)+eps())
ginv(s) = cbrt(s/0.5e-6)

"""
   zs(s, t, pg::Valley)
Surface elevation
"""
zs(s, t, pg::Valley) = (100*sqrt.(sqrt.((s+200))) + 1/60*s - 2e10^(1/4) + 1) # original function


"""
   zb(s, t, pg::Valley)
Bed elevation
"""
function zb(s, t, pg::Valley)
    sg = pg.sgrid[end]

    (zs(sg,t,pg) - pg.para*sg)/sg^2 * s^2 + pg.para*s
end

"""
   width(s, t, pg::Valley)

Glacier width
"""
width(s,t,pg::Valley) = ginv( (zs(s,t,pg)-f(s,t,pg))/(h(s,t,pg)+eps()) )*2



##########
# Degree day melt forcing
##########




# includes parameters that are defined in "Phys".. TODO make more universal.
"""
Daily temperature variations
"""
day_var(t, pp) = pp.DDAMP * cos(2*pi/day*t)

"""
    temperature(t, pp)

Seasonal temperature.  Mid-winter at t=0.
"""
function temperature(t, pp)

    if typeof(pp.ΔT) == Float64
        ΔT = pp.ΔT
    else
        ΔT = pp.ΔT(t)
    end
    out =-pp.DAMP.*cos(2*pi/year*t)-5 + ΔT
end


"""
    temperature_d(t, pp)

Daily and seasonal temperature.  Mid-winter/night at t=0.
"""
temperature_d(t, pp)=  temperature(t,pp) + day_var(t,pp)

"""
    temperature_d_ext(t, pp)

Daily maximal and minimal temperature.  Mid-winter/night at t=0.
"""
temperature_d_ext(t, pp)=  (t=temperature(t,pp); (t+pp.DDAMP,t-pp.DDAMP))

"""
    source_water_ddm(s,t,pg,pp)

Degree day melt source [m/s]. Uses `temperature_d` as temperature function.
"""
function source_water_ddm(s,t,pg,pp)

    melt  = max_smooth(0, ( zs(s,t,pg)*pp.lr+temperature_d(t,pp))*pp.DDF, pp.basal/2 )+ pp.basal

end

"""
    basal_velocity(s,t,pg,pp)

basal velocity as a function of deformation speed given SIA

TODO: make seperate function to improve coupling to ISSM
"""
function basal_velocity(s, t, pg,pp)

    @unpack fsl = pg
    alpha = gradzs(s, t, pg)

    ud = 2*A/(glen+1)*(0.8*ri*g*sin(alpha))^glen*(thick(s,t,pg)^(glen+1)) # surface velcity with shape factor assumption for a semi-elspes with ratio of 3...

        ub = fsl*ud # basal velocity
    return ub
end

"""
erosion relationship with velocity
"""
erosion(s,t,pg,pp) = 1/10^4*abs(basal_velocity(s,t,pg,pp))

"""
    erosion_herman(s,t,pg,pp; kg=2.7e-7, l=2.02)

erosion relationship with velocity based upon Herman et al, 2015
kg and l are constants given in Fig 3 of Herman et al, 2015
"""
function erosion_herman(s,t,pg,pp)
    ub = basal_velocity(s,t,pg,pp)*year

    out = (pp.Kg  # convert for time units (year -> sec) and meters in Herman et al, 2015 law
           *abs(ub)^pp.l_er
           )
    out= out/year
end
"""
    Rh_hooke(s,t,pg,pp)

find hydraulic radius from stuff... equation from Mauro's thesis
"""
function Rh_hooke(s,t,pg,pp,pn)
    @unpack hookeangle = pp

    S=xsect(s,t,pg,pp,pn)

    Rh=(sqrt(S*(hookeangle-sin(hookeangle)))/
        (sqrt(2)*(hookeangle+sqrt(2-2*cos(hookeangle))))
        )

    return Rh
end

"""
    manning2darcy_fric(s,t,pg,pp,pn,n)

convert manning friction factor (n) to Darcy-Weissbach using Equation B9 in Clarke et al., 2003
"""
function manning2darcy_fric(s,t,pg,pp,pn,n)

    fr =( (8*g*n.^2)
          .\
          Rh_hooke(s,t,pg,pp,pn).^(1/3)
          )

    return fr
end

"""
    begin_sp, end_sp = find_sp(global_date,pg)

Find the begining and end indexes of study period given external (sediment) data
"""
function find_sp(global_date,pg)
    begin_sp=0

    end_sp = 0

    for i=1:length(pg.tout)
        if abs(global_date[1]-pg.tout[i]) <= (pg.tout[2]-pg.tout[1])
            begin_sp=i+10
        end
        if abs(global_date[end]-pg.tout[i]) <= (pg.tout[2]-pg.tout[1])
            end_sp=i-10
        end
    end

    return begin_sp, end_sp
end

"""
    NSE(model,data)

Nash-Sutcliff Efficiency Coefficent

returns values between -Inf and 1, one begin perfect fit.
"""
function NSE(model,data)

    E = abs.(model)-abs.(data)
    SSE = sum(E.^2)
    U = mean(abs.(data))

    SSU = sum((abs.(data) .- U).^2)

    out = 1-SSE/SSU

    return out
end

"""
    NSE(model,data)

Nash-Sutcliff Efficiency Coefficent with log on input to minimize the effects of low flow situations

from something by Duethmann et al WRR 2015

returns values between -Inf and 1, one begin perfect fit.
"""
function NSE_log(model,data)
    thresh = 1e-3

    model_= []
    data_ = []
    # get rid of unreasonable values... but slows the thing down.
    for i= 1:length(model)
        if model[i] > thresh && data[i] > thresh
            push!(model_, model[i])
            push!(data_, data[i])
        end
    end

    E = log.(model_)-log.(data_)
    SSE = sum(E.^2)
    U = mean(log.(data_))
    SSU = sum((log.(data_)-U).^2)

    out = 1-SSE/SSU

    return out
end

"""
minimize_for_DT(ΔT, ice_melt, t_period,pg,pp;verbose=false)

cost function to determine ΔT in PDD model given discharge over a certain time period
"""
function minimize_for_DT(ΔT, ice_melt, t_period,pg,pp; verbose=false)
    pp = Phys(pp,
              DT=ΔT[1]
              )

    dt = .2*day
    times = t_period[1]:dt:t_period[end]
    discharge = zeros(length(times))
    for i in eachindex(times)
       discharge[i] = abs(SUGSET.Q_w(pg.sgrid,times[i],pg,pp)[1])*dt
    end

    out =  (ice_melt) .- sum(discharge)
    verbose && println(" discharge: $(round(sum(discharge),digits=4))... cost: $(round(out,digits=4))... ΔT: $(round(ΔT[1],digits=4))")

    return out
end

"""
find_ΔT(ΔT, time_bracket,  t)

returns value of DT given time t, likely from minimize_for_DT()

"""
function find_ΔT(ΔT, time_bracket,  t)

    for i=1:length(time_bracket)-1
        if t< time_bracket[1]
            out = ΔT[1]
            break

        elseif t< time_bracket[end]
            out = ΔT[end]
            break

        elseif t < time_bracket[i] && t < time_bracket[i+1]
            out = ΔT[i]
            break
        end
    end

    return out
end


"""
FQ1 from Duethmann 2015
"""
function fq1(model,data)
    return 0.5*(NSE(model,data)+NSE_log(model,data))
end




"""
    b_0_determ(Q,pg)

Q sorter implemented to get rid of low values in measurements (<0.5)

Returns array b_0 with mass balances at terminous with given discharge by calling  q_distrib(sgrid, b_0, Q,pg)

Formally include a bisector relationship, but find_zero() from Roots seems to be faster.

Note that tolerance must be small as this solves for melt at terminous in m/s
"""
function b_0_determ(Q,pg)
    b_0 = zeros(length(Q))

    @showprogress for i = 1:length(b_0)
        println(i)
        Q[i]<0.1 && continue
        b_0[i]    = Roots.fzero(b_0 -> q_distrib(pg.sgrid, b_0, Q[i],pg), b_0_rng(pg.sgrid*0.1, Q[i],pg); xtol=1e-11)
    end

    return b_0
end


"""
    q_distrib(s,b_0,Q,pg)

Function to be optimized.
- s is spatial decretization to determine surface area of glacier
- b_0 is mass balance of glacier terminous
- Q is discharge.
- pg are parameters, in this case they contail the mass balance gradient gamma

in function tmp is mass- balance for a given B_0 in a cell,
the sum of which is subtracted from discharge
"""
function q_distrib(s,b_0,Q,pg)
    ds = pg.sgrid[2]-pg.sgrid[1]

    tmp = zeros(1,length(s))
    for i=1:length(s)
        ds = pg.sgrid[2]-pg.sgrid[1]
        tmp[i] = width(s[i],0.0,pg).*ds.*max(0,(b_0 .+ pg.gamma.*(zs(s[i],0,pg)-zs(pg.sgrid[1],0,pg))))
    end

    return sum(tmp)-Q
end
"""
    b_0_rng(s,Q,pg)

is the maximum parameter space for the root algorithm, where by all discharge,Q, originates solely from the last cell.
"""
function b_0_rng(s,Q,pg)
    ds =(s[2]-s[1])
    return Q./(ds.*width(s[1],0.0,pg))
end

"""
Get cell center coordinates and cell size.
"""
function get_smid_ds(sgrid)
    smid = zeros(length(sgrid)-1)
    ds = zeros(length(sgrid)-1)
    for i = length(sgrid)-1:-1:1
        smid[i] = (sgrid[i+1] + sgrid[i])/2
        ds[i] = sgrid[i+1] - sgrid[i]
    end
    smid, ds
end

h0_fn(x, t_h, pg) = x * t_h / pg.sgrid[1]

"Move cell center values onto grid. Probably discharge first and last."
ongrid(hts) = (([0; hts] .+ [hts; 0])/2)

"""
    function to test model performance to a reference run with regard to solver tolerance and cell-size

    tol_tester(grid_num, tol, pg_ref,pn_ref, pg)...

    grid_num is number of cells (array)
    tol is the tolerance to be tested (array)
    pg_ref and pn_ref are parameters of a reference run

"""
function tol_tester(GlacierType::DataType, grid_num, tol, pg, pp, pn, experiment, htic)
    used_norm = 1

    grid_num_ref = (maximum(grid_num)-1)*2 + 1
    tol_ref = minimum(tol)/10

    if experiment == "hi-res"
        domain = GlacierType(pg,
                             tspan=pg.spin_time_span).domain

        pg_ref = GlacierType(pg,
                            tspan=pg.spin_time_span,
                            spin = true,
                            sgrid=range(pg.domain[1],stop= pg.domain[2], length=grid_num_ref),
                            # initial till layer thickness increasing from 0 to htic
                            ht0=h0_fn.(range(pg.domain[1], stop=pg.domain[2], length=grid_num_ref)[1:end-1],htic,pg)
                            )

        else
            domain = GlacierType(pg,
                                 tspan=pg.tspan).domain
println(grid_num_ref)
            pg_ref = GlacierType(pg,
                                 tspan=pg.tspan,
                                 spin = true,
                                 sgrid=range(pg.domain[1], stop= pg.domain[2],length= grid_num_ref),
                                 # initial till layer thickness increasing from 0 to htic
                                 ht0=h0_fn(range(pg.domain[1],stop= pg.domain[2], length =grid_num_ref)[1:end-1],htic,pg)
                                 )
    end

    odeopts = Num().odeopts # get defaults
    odeopts[:abstol] =  odeopts[:reltol]= tol_ref

    odeopts[:dtmax] = day/2
    pn_ref = Num(#alg=DifferentialEquations.Midpoint(),
                 odeopts=odeopts)

    println("Calculating reference solution with $(length(pg_ref.sgrid)) grid points and tol $(odeopts[:abstol]):")
    @time sol_ref = run_model(pg_ref, pp, pn_ref)
    hts_ref = sol_ref(pg_ref.tspan[2])

    hts_ref_ongrid = ongrid(hts_ref)


    ########################
    #### comparison here ###
    ########################
    grid_num_ref = length(pg_ref.sgrid)

    st_ref = (grid_num_ref-1) ÷ (minimum(grid_num) -1)

    error_array_mean = NaN*zeros( length(grid_num),length(tol))
    error_array_max = NaN*zeros( length(grid_num),length(tol))

    ΔV_array =NaN*zeros( length(grid_num),length(tol))

    for i = 1:length(grid_num)

        st = (grid_num[i] -1) ÷ (minimum(grid_num) -1)

        for j = 1:length(tol)
            print("Grid num $(grid_num[i]), Grid size $(pg.sgrid[end]./grid_num[i]), Tolerance $(tol[j]) ...")

            if experiment == "hi-res"
                pg = GlacierType(pg_ref, ## makes sure that parameters are similar to ref run.
                                 tspan =pg.tspan,
                                 #spin = true, # using spin runs here
                                 sgrid=range(pg_ref.domain[1],stop= pg_ref.domain[2],length=grid_num[i]),
                                 # IC & BC
                                 ht0=h0_fn(range(pg_ref.domain[1], stop=pg_ref.domain[2], length=grid_num[i])[1:end-1],htic, pg)
                                 )
            else
                pg = GlacierType(pg_ref,
                                 tspan =pg.tspan,
                                 sgrid=range(pg_ref.domain[1],stop=pg_ref.domain[2],length=grid_num[i]),
                                 # IC & BC
                                 ht0=h0_fn(range(pg_ref.domain[1], stop=pg_ref.domain[2], length=grid_num[i])[1:end-1],htic,pg)
                                 )
            end

            odeopts[:abstol] = odeopts[:reltol] = tol[j]

            pn = Num(pn_ref,
                     odeopts = odeopts
                     )

            sol, walltime  =  @timed run_model(pg, pp, pn) # run model

            println(" took $walltime seconds")
            hts = ongrid(sol(pg.tspan[2]))

            ΔV_array[i,j] = SUGSET.mass_con_check(sol, pg, pp, pn)[3]./((pg.tout[end]-pg.tout[1])./year)

            @assert all(pg.sgrid[1:st:end].==pg_ref.sgrid[1:st_ref:end])
            @assert used_norm == 1

            error_array_mean[i,j] = norm( ( (hts[1:st:end] - hts_ref_ongrid[1:st_ref:end]) )[2:end-1], used_norm)./(length(hts[1:st:end][2:end-1])* (pg.tout[end]-pg.tout[1])./year)

            error_array_max[i,j] = norm( ( (hts[1:st:end] - hts_ref_ongrid[1:st_ref:end]) )[2:end-1], Inf)./((pg.tout[end]-pg.tout[1])./year)

        end
    end

    return error_array_mean, error_array_max, ΔV_array, pg_ref
end


"""
spinner(GlacierType,pg,pp,pn,threshold)

runs model initially, then updates hts until THRESHOLD is met.
returns array of hts for use in pg.ht0

"""
function spinner(GlacierType::DataType,pg,pp,pn,threshold,break_time) #TODO make spinner pointwise and return year
    @unpack tout, tspan, sgrid = pg


    pg= GlacierType(pg,
                    tspan= pg.tspan_spin)
    pg= GlacierType(pg,
                    tout=pg.tspan[1]:day:pg.tspan[2]
                    )

    tout = pg.tout

    # runs model
    sol = run_model(pg, pp, pn)

    hts = sol(tout)
    hts_evol = hcat(hts...)

    dt = (tout[end]-tout[1])./year

    i = 1
    runs = 0

    while abs(mean(hts_evol[:,end])- mean(hts_evol[:,1]))/dt > threshold #&& i < 15 # mean  change over approximatey one year
       # println("loop $(i) started")

        #println("yearly change:  $((mean(hts_evol[:,end])- mean(hts_evol[:,1]))/dt)
         #       threshold: $(threshold)")
#
        i += 1

        if i >= break_time

            break
        end

        pg=GlacierType(pg,
                       spin=true,
                       ht0=hts_evol[:,end],
                       )

        # println("mean ht0: $(mean(pg.ht0))
        #         spin fac: $(pg.spin_fac)
        #         spin? $(pg.spin)"
        #         )
        runs = i
        sol = run_model(pg, pp, pn) # runs model
        hts_evol = sol(tout)
    end

    out = hts_evol[:,end]
    sol =[]; hts_evol=[]; hts=[]; pg = []; pp=[]; pn=[]; dt=[]

    return out , runs
end


"""
spinner_equilibrium(GlacierType,pg,pp,pn,threshold,percent_diff)

runs model initially, then updates hts until THRESHOLD is met.
returns array of hts for use in pg.ht0

"""
function spinner_equilibrium(GlacierType::DataType,pg,pp,pn,threshold,percent_diff)
    @unpack tout, tspan, sgrid = pg


    pg= GlacierType(pg,
                    tspan= pg.tspan_spin)
    pg= GlacierType(pg,
                    tout=pg.tspan[1]:day:pg.tspan[2]
                    )

    tout = pg.tout

    # runs model
    sol = run_model(pg, pp, pn)

    hts = sol(tout)
    hts_evol = hcat(hts...)

    dt = (tout[end]-tout[1])./year

    i = 1
    runs = 0

    ##########################################
    ## find equilibrium for Qbs and source) ##
    ##########################################
    smid, ds = get_smid_ds(sgrid)
    # glacier width (assumes fixed topo)
    w = width.(smid, Ref((tout[end]-tout[1])*0.5), Ref(pg))
    Δhts = sol(tout[end]) - sol(tout[1])

    ## discharge ##
    ΔV = sum(Δhts.*w.*ds)

    V_source = 0
    ## source ##
    for (i,t) in enumerate(tout)

        ht = sol(t)

        V_source += sum(effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), ht).* width.(smid, Ref(t), Ref(pg)).*ds*dt)

    end

    sed_storage = 1 - (V_source/ΔV)




    while abs(mean(hts_evol[:,end])- mean(hts_evol[:,1]))/dt > threshold && sed_storage > percent_diff
        println("loop $(i) started")

        i += 1

        if i == 100

            break
        end

        pg=GlacierType(pg,
                       spin=true,
                       ht0=hts_evol[:,end],
                       )


        println("mean ht0: $(mean(pg.ht0))
                 spin? $(pg.spin)"
                )
        runs = i
        sol = run_model(pg, pp, pn) # runs model
        hts_evol = sol(tout)

        ##########################################
        ## find equilibrium for Qbs and source) ##
        ##########################################
        smid, ds = get_smid_ds(sgrid)
        # glacier width (assumes fixed topo)
        w = width.(smid, Ref((tout[end]-tout[1])*0.5), Ref(pg))
        Δhts = sol(tout[end]) - sol(tout[1])

        ## discharge ##
        ΔV = sum(Δhts.*w.*ds)

        ## source ##
        for (i,t) in enumerate(tout)

            ht = sol(t)

            V_source += sum(effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), ht).* width.(smid, Ref(t), Ref(pg)).*ds)*(pg.tout[2]-pg.tout[1])

        end

        ė= V_source

        sed_storage = 1-min(abs(ΔV/ė) , abs(ė/ΔV))

        ##############

        println("yearly change:  $((mean(hts_evol[:,end])- mean(hts_evol[:,1]))/dt)
                threshold: $(threshold)
                sed storage: $(sed_storage)
                percent_diff: $(percent_diff)
                ΔV: $(ΔV)
                ė: $(ė)
                "
                )



    end

    println("spin ended")
    return hts_evol[:,end], runs
end
