module SUGSET
# Ian Delaney's glacier sediment yield model
using Parameters, OrdinaryDiffEq, ProgressMeter, Interpolations,DifferentialEquations
using VAWTools
using Statistics
using ForwardDiff
export Glacier, Phys, Num, run_model, day, year, L, g, cw, rw, ri, glen, A

## auxillary.jl and post_processing.jl included at end

# Misc. well-known, fixed physical constants
const day = 24*3600. # secs in day
const year = day*365 # secs in year
const L = 3.34e5  # lat. heat of fusion
const g = 9.81 # grav. accel.
const cw = 4220.  # heat cap.
const rw = 1000.  # dens water
const ri = 900.   # dens ice
const glen = 3
const A = 2.4e-24

# Some other constants
const proglacial = 10   # ice thickness when proglacial starts... and dhdt = 0

"""
Physical parameters with their defaults.
"""
@with_kw struct Phys @deftype Float64
    ## Hydraulic
    beta = 3/2
    alpha = 5/4
    k = 0.05
    fi = 0.175	# assuming the same as above
    ft = 0.175	# assuming the same as above
    @assert fi==ft # needed in current code
    xsec_min = 0.3 # minimum value of channel xsect
    hookeangle = pi/6 # angle of Hooke channel xsect (Hooke et al 1990)

    ## Bedload transport
    rs = 2650 	# dens sed
    R  = (rs-rw)/rw # specific gravity of submerged sediment
    Dm = .1	# median grain size of sed.
    l = 100 # empirical lag distance for sediment flux to reach equilibrium load
    formula::Symbol = [:MPM, :Smart, :DuBoys, :Kalinske, :E_H][end]

    ## Till production
    l_er = 2.02 # erosion exponent in power-law model from Herman 2015
    Kg = 2.7e-7 # erosion constant in power-law model from Herman 2015

    ## Melt model
    lr = -0.0075  #K/m lapse rate
    ΔT::Any = 0.0 # temp offset
    DAMP = 16 # temp amplitude
    DDAMP = 1
    DDF = 0.01/day # degree day factor
    DDF_Qdet = 0.0000001
    basal= 7.93e-11 #wm/s
end

"""
Some ODE solvers to choose from.
- Adam-Bashforth methods seem to work best with VCABM the best

See http://docs.juliadiffeq.org/latest/solvers/ode_solve.html
"""
const solvers = [BS5(), VCABM(), JVODE_Adams()]

"""
Numeric parameters
"""
@with_kw struct Num @deftype Float64
    # ODE solver,
    # http://docs.juliadiffeq.org/latest/solvers/ode_solve.html
    alg::Any= VCABM()
    # Solver options
    # http://docs.juliadiffeq.org/latest/basics/common_solver_opts.html
    odeopts::Dict{Symbol,Any} =
        Dict{Symbol,Any}(
            :abstol=>1e-7, # catchment erosion rates are ~1mm/year ~ 1e-11m/s, thus h needs to be able to resolve this
            :reltol=>1e-7,
            :dtmax=> 0.25*day, # maximum step size.  Set such that all temporal forcing is resolved.
            :save_everystep=>true # might make faster...
        )
    # misc smoothers, may make solver faster:
    Δσ = 0.001 # smooth mobilization when going from transport to supply limited over this range of ht [m]
    max_delta_xsect = [0, 0.01][2]
    max_delta_MPM = [0, 0.001][1]
    max_delta_dht_dt_fn = [0, 1e-13][1]
end

#######

"""
Supertype of all glaciers

Requires:
    tspan - time span
    tout - output times
    sgrid - standard grid which defines glacier (cell boundaries not cell-centers)
    float_frac - flotation fraction
    Q_top - water discharge entering domain from top
    till_lim # limit at which till stops growing
    till_growth_lim # limit of till height
    ht0 # initial till height
    fsl # basal sliding fraction
    spin # whether in spinning-up state or not
    spin_fac # spin-up speed-up factor
    source_average_time # time period over which to look at a
                        # smoothed water source to calculate xsect
    source_quantile # what quantile to take
"""
abstract type Glacier end

"""
   zb(s, t, pg::Glacier)
Bed elevation
"""
function zb end

"""
   zs(s, t, pg::Glacier)
Surface elevation
"""
function zs end

"""
   thick(s, t, pg::Glacier)
Ice thickness
"""
thick(s, t, pg::Glacier) = zs(s,t,pg) - zb(s,t,pg)

"""
   width(s, t, pg::Glacier)

Glacier width
"""
function width end

"""
    source_water(s, t, pg::Glacier, pp::Phys)

Water source term in m/s.
"""
function source_water end

"""
    source_water_sm(s, t, pg::Glacier, pp::Phys)

Smoothed water source term in m/s.  The smoothing should be (almost)
causal, i.e. only take the source at previous times into account.

Note: you can provide your own implementation of this (e.g. for
performance reasons) by overloading on `pg`.
"""
function source_water_sm(s, t, pg, pp)
    sat = pg.source_average_time
    source_quantile = pg.source_quantile
    # average over some time
    dt = 0.075*day

    set = zeros(convert(Int,div(sat,dt)))
    for i = 1: length(set)
        tt = t-sat .+ dt*i
        set[i] = source_water(s,tt, pg,pp )
    end
    out = quantile(set, source_quantile)
end

"""
    source_till(s, t, pg::Glacier, pp::Phys)

Till production term in m/s
"""
source_till(s,t,pg,pp) = erosion(s,t,pg,pp)



## gradients for surface and bed

"Gradient of bed elevation"
gradzb(s, t, pg) = ForwardDiff.derivative(s->zb(s,t,pg), s)

"Gradient of surface elevation"
gradzs(s, t, pg) = ForwardDiff.derivative(s->zs(s,t,pg), s)

"Gradient of width"
gradwidth(s, t, pg) = ForwardDiff.derivative(s->width(s,t,pg), s)

##############
# Hydraulics Model
##############
#
# Hydraulic potential is some algebraic function of topography,
# e.g. overburden potential.
#
# Note that the hydraulic model could be
# swapped out easily as long as there is not feedback between sediment
# discharge and hydraulics.

##### find hydraulic potential #####
"""
    dphidx(s,t,pg,pp::Phys)

Gives hydraulic gradient approximation using of Shreve potential (with
a modification using `pg.float_frac`).
"""
Ψˣ(s,t,pg,pp::Phys) = pg.float_fracˣ*ri*g*(gradzs(s,t,pg) - gradzb(s,t,pg))  + rw*g*gradzb(s,t,pg)

"""
    Q_w(sgrid, t, pg, pp)

Calculate discharge from a source `source_water(s, t, pg, pp)` by
integrating from the top.
"""
Q_w(sgrid, t, pg, pp) = _Q_w(sgrid, t, pg, pp, source_water)

"""
    Q_w_sm(sgrid::AbstractVector, t::Number, pg, pp)

Smoothed discharge used to calculate the channel size.
"""
Q_w_sm(sgrid, t, pg, pp) = _Q_w(sgrid, t, pg, pp, source_water_sm)

function _Q_w(sgrid::AbstractVector, t::Number, pg, pp, source_fn)
    @unpack basal = pp

    Qw = zeros(typeof(t), length(sgrid))

    # integrate (sum) from the top
    Qw[end] = pg.Q_top

    for i=length(sgrid)-1:-1:1
        smid = (sgrid[i+1] + sgrid[i])/2 #on staggered grid
        ds = sgrid[i] - sgrid[i+1] # note Qw is negative if flow is to the left
        if thick(smid,t,pg) <= proglacial
            Qw[i] = Qw[i+1] # do not add melt if there is no glacier
        else
            Qw[i] = (source_fn(smid, t, pg, pp)+basal) * width(smid,t,pg) * ds + Qw[i+1]
        end
    end

    return Qw
end

"""
To account for Hooke-channels in the Darcy-Weisbach equation.

For a circle this is 1/pi^2, for semi-circle 4*pi^2/(pi+2)^4.
"""
hookefact(hookeangle) = ( 2*(hookeangle - sin(hookeangle)) /
                          (hookeangle + 2*sin(hookeangle/2))^2 )^2

"Hyd diameter from Darcy-Weisbach"
Dh(dphi, Q⁺, pg, pp) = (pp.fi * 8*rw*(Q⁺)^2/abs(dphi) * hookefact(pp.hookeangle) )^(1/5)

"Convert hyd. diameter to cross-sectional area"
function Dh2S(Dh, hookeangle)
    (Dh/4)^2 * (sqrt(2) * (hookeangle + sqrt(2 - 2*cos(hookeangle))))^2 /
        (hookeangle - sin(hookeangle))
end

"Hyd dia of Hooke channel (Werder & Funk 2009)"
function S2Dh(S, hookeangle)
    4* sqrt(S*(hookeangle - sin(hookeangle))) /
         (sqrt(2) * (hookeangle + sqrt(2 - 2*cos(hookeangle))))
end

###### get xsection #########
"""
    xsect(sgrid::AbstractVector, t::Number, pg, pp, pn)

Cross sectional area S of channel on all grid-points by solving flux
relation Q=-kS^α √Ψ for S

NOTE: Smoothed discharge Q_w_sm is used in this calculation
"""
function xsect(sgrid::AbstractVector, t::Number, pg, pp, pn)
    @unpack beta, alpha, xsec_min, hookeangle, fi = pp
    S = Q_w_sm(sgrid,t,pg,pp) # also use for output
    Dhs=Q_w_sm(sgrid,t,pg,pp) # also use for output
    
    for i=1:length(S)
        qw = S[i]
        s_ = sgrid[i]
        dphi = Ψˣ(s_,t,pg,pp)

        _Dh = Dh(dphi, qw, pg, pp)

        S[i] =  VAWTools.max_smooth(xsec_min, Dh2S.(_Dh, hookeangle),pn.max_delta_xsect)
        Dhs[i] =  VAWTools.max_smooth(xsec_min, S2Dh.(_Dh, hookeangle),pn.max_delta_xsect)

    end

    return S, Dhs
end

"""
Width of channel floor for a given cross-sectional area.
"""
channel_width(S, hookeangle) = 2*sqrt(2*S/(hookeangle-sin(hookeangle)))*sin(hookeangle/2)

###### find water shear stress #######

"""
    shr_str_w(sgrid::AbstractVector, t::Number, pg, pp, pn)

Shear stress of water on sediment according to Darcy-Weisbach.

NOTE: uses the un-smoothed discharge.
"""
function shr_str_w(sgrid::AbstractVector, t::Number, pg, pp, pn)
    @unpack fi,ft, xsec_min = pp
    @assert fi==ft "Not implemented"
    
    S,Dhs=xsect(sgrid,t,pg,pp,pn) 
    q_w = Q_w(sgrid,t,pg,pp)
    tau = copy(S)

    for i= 1:length(tau)
        v = q_w[i]/S[i]
        tau[i] = 1/8*fi*rw*v^2
    end

    return tau, q_w, S,Dhs
end

######################
# Sediment erosion & transport
######################

###
# Bedload transport capacity per unit channel width [m2/s]
###
#
# Maybe also implement: Smart-Jaeggi, Du Boys, Kalinske

"""
    MPM(sgrid, t::Number, pg, pp, pn)

The Meyer-Peter Müller bedload transport capacity formula.  (See Wong
& Parker eq. 24)
"""
function MPM(sgrid, t::Number, pg, pp, pn)
    @unpack R, Dm = pp
    tau, q_w, S,Dhs =  shr_str_w(sgrid,t,pg,pp,pn)

    out = tau # modify this array in-place and use as output
    for i=1:length(tau)
        excess_shear =  VAWTools.max_smooth(0, tau[i]/(rw*R*g*Dm) - 0.0495,
                                   pn.max_delta_MPM)
        dir = sign(q_w[i])
        # scaling uses W&P eq 3
        out[i] = 3.97* dir *sqrt(R*g*Dm^3*excess_shear^3)
    end
    return out, q_w, S,Dhs
end

"""
    E_H(sgrid, t::Number, pg, pp, pn)

Engelund and Hansen 1967... Total sediment transport relationship.

https://www.engr.colostate.edu/~pierre/ce_old/Projects/CASC2D-SED%20Web%20site%20082506/CASC2D-SED-Sediment.htm
http://sic.g-eau.net/engelund-hansen-1967,1033?lang=en
https://www.tugraz.at/fileadmin/user_upload/Institute/IWB/Lehre/Software/BedLoadAnalyzer/Referenzhandbuch.pdf
"""
function E_H(sgrid, t::Number, pg, pp, pn)
    @unpack R,fi,Dm,rs = pp

    tau, q_w, S,Dhs =  shr_str_w(sgrid,t,pg,pp,pn)

    velo = sqrt.(8*tau./fi)
    dir = sign.(q_w)
    # E.g. https://www.tugraz.at/fileadmin/user_upload/Institute/IWB/Lehre/Software/BedLoadAnalyzer/Referenzhandbuch.pdf
    # equation 45
    qb = dir.* (0.05*8/fi) * (1/(Dm*R^2*g^2)) .*(tau./rw).^(5/2)


    return  qb, q_w, S,Dhs
end


Ψ(Dh, Q, pp) = 8*rw*pp.fi*Q^2 / Dh^5 * hookefact(pp.hookeangle)

"""
    sed_transport(sgrid::AbstractVector, t::Number, pg,pp)

Equilibrium bedload transport rate [m³/s]

Select sediment transport relationship by setting pp.formula.
he called functions should give transport per unit channel width in physical (SI) units.
"""
function sed_transport(sgrid::AbstractVector, t::Number, pg, pp, pn) #make universal so other sed. transport formulas can be added.
    # shields, q_w, S =  shr_str_w(sgrid,t,pg,pp,pn)
    qbe, q_w, S,Dhs = if pp.formula==:MPM
        MPM(sgrid, t, pg, pp, pn)
    elseif pp.formula==:E_H
        E_H(sgrid, t, pg, pp, pn)
    else
        error()
    end
    cw = channel_width.(S,pp.hookeangle)

    return qbe.*cw, q_w, S,Dhs
end

"""
Sediment source function.  Goes linearly to zero as ht->pg.till_growth_lim.
"""
function effective_sed_source(s, t, pg, pp, pn, ht)
    @unpack till_growth_lim, spin, spin_fac = pg
    if thick(s,t,pg) <= proglacial # no glacier
        return 0.0
    else
        out = source_till(s, t, pg, pp) * (1 - min(1,max(0,ht/till_growth_lim)))
    end
    return out
end



"""
# Mobilization (volume per unit time per unit channel lenxgth) occurs if transport
        # is below the transport capacity (otherwise deposition happens).
        # This corresponds to the dQb/dx term in the Exner equation.
        #
        # Note:
        # - we use the sediment flux a the upstream cell
        #   boundary to calculate cell-average mobilization.  This is a form up upwinding.
        # - porosity is taken as zero

      mobilization_return(Qbe,Qin,hti,pp)    
"""
function Qb_dhdt_return(Qbe,Qin,hti,mt,thickness,w,pg,pp,pn)

    ds= abs(pg.sgrid[2]-pg.sgrid[1])
    @assert Qin>=0
    
    @assert mt>=0
    
    mobilization_ = (abs(Qbe) - abs(Qin))/pp.l
  
    ############
 
    #@assert sign(Qb)==sign(q_w) ||abs(Qb)<1e-5*spin_fac  "assert 1: $(Qb), $(q_w), $j"
    #@assert sign(Qbe)==sign(q_w) || Qbe≈0 "assert 2: $(sign(Qbe)), $(sign(q_w)), time: $t  , $j"
    
    
    # conditions A, B & C
     if hti>= pg.till_lim && mobilization_<=0 # Condition C: fudge to keep ht<till_lim
         mobilization =   zero(mobilization_)
         
     elseif thickness <= proglacial # no glacier: keep transport steady
         mobilization =  zero(mobilization_)
         
     else # Condition A (transport limited) or B (supply limited)
         if mobilization_<=mt*w # transport limited for sure
             mobilization = mobilization_
             
         else # Either transport or supply limited,

             # make a smooth transition between the two:
             trans = VAWTools.sigmoid(hti, 0+2*pn.Δσ, pn.Δσ)            
             #println(trans)
             if hti < 1e-5
                 trans=0
             end
             
             mobilization =mobilization_*trans + (1-trans)*mt*w                   
         end
     end

    Qout  = (Qin + mobilization*ds) # sed-flux out of cell
    
    @assert Qout>=0 "Qin:  $(Qin)...  Qout: $(Qout)... ... mobilization: $(mobilization)"
    
    Qb = -Qout
    
    # Exner equation:
    dht_dt =  BigFloat((Qin-Qout)/(w*ds) + mt) # [m/s]

    return Qb, dht_dt
end


"""
    dht_dt_fn(ht, sgrid, t, pg, pp, pn)

Rate of change of till layer thickness (and bedload flux).
"""
function dht_dt_fn(ht, sgrid, t, pg, pp, pn, dht_dt = zeros(Float64, length(sgrid)-1)) # consider making non- allocating? dht_dt_fn!(ht, sgrid, t, pg, pp, pn, dht_dt = zeros(Float64, length(sgrid)-1))
    # rate of change of till layer thickness
    @unpack l = pp
    @unpack spin, spin_fac, Qb_top, sgrid, till_lim = pg
    
    # equilibrium bedload TODO: and ht and change dphi()
    Qbe, q_w, S = sed_transport(sgrid, t, pg, pp, pn)

       #println("time $(t/day)")
    if pg.stoch == true
        for i = 1:length(Qbe)
            Qbe[i] =  Qbe[i]*((1-.5)* rand(RandomNumbers.Xorshifts.Xoroshiro128(Int(floor((t÷3600)*i))))+.5) # sed-flux out of cell
        end
    end
    
    # # figure out an eltype such that DiffEq can do auto-diff for stiff solvers:
    # T = eltype(source_till(sgrid[1], t, pg, pp).*(till_lim-ht[1])
    #            + (abs(Qbe[1+1]))/(l))
    T = eltype(Qbe)

    Qb = zeros(T, length(sgrid)) # actual bed load
    Qb[end] = Qb_top # boundary condition

    # integrate from top
    for i=length(sgrid)-1:-1:1
        hti = ht[i]
        ds = abs(sgrid[i] - sgrid[i+1])
        smid = (sgrid[i+1] + sgrid[i])/2
        w = width(smid,t,pg) # glacier width
        
        mt = BigFloat(effective_sed_source(smid, t, pg, pp, pn, hti))
        
        thickness = thick(smid,t,pg)

        Qb[i], dht_dt[i] = Qb_dhdt_return(Qbe[i],-Qb[i+1],hti,mt,thickness,w,pg,pp,pn)

    end

    return dht_dt, Qb, Qbe, q_w
end

##############
# The whole model
##############
"""
    hts, dht_dts, Qws, Qws_sm, dphi_dx, Qbs, Qbes, sol, shr_str, shield_str = run_model(pg::Glacier, pp::Phys, pn::Num)

Run the model.
"""
function run_model(pg, pp::Phys, pn::Num; verbose=false)
    @assert maximum(diff(pg.sgrid)) <= pp.l*0.9 "Spatial discretisation needs to be smaller than pp.l"
    @unpack sgrid, ht0, tspan, tout = pg


    verbose && (print("Start integration ... "); tic())


    cb=DifferentialEquations.PositiveDomain()
    objfn = (y,p,t) -> dht_dt_fn(y, sgrid, t, pg, pp, pn)[1]
    prob = ODEProblem(objfn, ht0, tspan)
    sol = solve(prob, pn.alg;
                pn.odeopts...)

    verbose && (println("integration finished"); toc())

    return sol
end


include("auxillary.jl")
include("post_processing.jl")

end
