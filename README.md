# SUGSET

Various functions to describe hydrology, subglacial hydrology and sediment transport.

Runners and data read-in/parsing scripts are not include, except for a synthetic test-case in "example" directory.

To install:
Pkg.clone("git@bitbucket.org:IanDelaney/sugset.jl.git", "SUGSET")