# Bench-like synthetic run
using Parameters
using PyPlot
using DifferentialEquations
import Roots: find_zero
using StatsBase
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year,Num

#############################
### run model and process ###
#############################

tend = 365*5

pg = Valley(tspan=(90*day, 20*365* day),
            fsl=0,
            )
pn=Num()
pg = Valley(pg,
            tout =pg.tspan[1]:day:pg.tspan[2],
            ht0=pg.sgrid[1:end-1] .* 0 .+ 1e-3 # initial till layer thickness
            )

pp = Phys( ΔT=4.0,
           DAMP=16, # annual variation set to SHMIP
           DDAMP=1, #daily variation
           Dm=0.01,
          )

SUGSET.zs(s,t,pg) = .05*s + 200
SUGSET.zb(s,t,pg) = .05*s
SUGSET.width(s,t,pg) = 2000

println("starting run_model()")
println(Dates.now())
@time sol  = run_model(pg, pp, pn)
println("run_model() finished")

#_, _, till_model, _ =SUGSET.mass_con_check(sol,pg, pp, pn)

#hts, dht_dts, sed_source, Qws, Qws_sm, Qbs, Qbes  = post_proc(sol,pg, pp, pn)

println("post_proc() finished")

println("norm ts's $(length(pg.tout))")






subplot(3,1,1)
plot(sol.t[1:end-1]/year,diff(sol.t/3600), label="Δt max: $(hrs[i]) hrs")
#ylabel("time (years)", fontsize=15)
ylabel("time step (hrs)", fontsize=15)
tick_params(axis="both" ,labelsize=15*.9)
legend(loc =1, fontsize=15)

subplot(3,1,2)
    plot(sol.t/year, SUGSET.width.(Ref(pg.sgrid[1]),sol.t,Ref(pg)).*SUGSET.source_water.(Ref(pg.sgrid[1]),sol.t,Ref(pg),Ref(pp)),label="sum Qws $(round(-sum(Qws[1,:])*mean(diff(pg.tout)),digits=0)) m^3")
#ylabel("time (years)", fontsize=15)
ylabel(L"Q$_w$ source at grid 1 (m$^3$ s$^{-1}$)", fontsize=15)
    tick_params(axis="both" ,labelsize=15*.9)
legend(loc =1, fontsize=15)

subplot(3,1,3)
plot(pg.tout/year, -Qbs[1,:], label="discharge sum: $(round(-till_model,digits = 1)) m^3")
#ylabel("time (years)", fontsize=15)
ylabel(L"Q$_s$ (m$^3$ s$^{-1}$)", fontsize=15)
tick_params(axis="both" ,labelsize=15*.9)
legend(fontsize=15)

#end
