#check_pt=map(x->x÷5, [1070, 1270, 1380])[3]
check_pt = 50
# ###### plot it up #########
figure("time")
ax=subplot(3,1,1)#; hold(true)
plot(pg.tout/year, -Qws[1,:], linewidth="1")
plot(pg.tout/year, -Qws_sm[1,:], linewidth="1")
ylabel(L"$\mathrm{Q_w}$ at snout $(\mathrm{m^3s^{-1}})$",fontsize=12)
legend(["actual", "smoothed"],fontsize=12)
axvline(pg.tout[check_pt]./year)

subplot(3,1,2, sharex=ax); hold(true)
plot(pg.tout/year, -Qbes[1,:])
plot(pg.tout/year, -Qbs[1,:])
ylabel(L"$\mathrm{Q_s}$ near snout $(\mathrm{m^3s^{-1}})$",fontsize=12)
legend(["equil.", "actual"],fontsize=12)
axvline(pg.tout[check_pt]./year)

# smid=zeros(length(pg.sgrid)-1)
# for i =length(pg.sgrid)-1:-1:1
#     smid[i] = (pg.sgrid[i+1] + pg.sgrid[i])/2 #non stagard grid
# end
# w=zeros(length(smid))
# for i = 1:length(w)
#     w[i] = width(smid[i], 0, pg)
# end

# ax=subplot(3,1,3,sharex=ax); hold(true)
# t=0
# tmp = (s,t) -> source_till(s,t,pg,pp)
# source_t = tmp.(pg.sgrid[1:end-1]+step(pg.sgrid)/2, pg.tout')
# plot(pg.tout/year, sum((dht_dts-source_t).*w.*diff(pg.sgrid) , 1)')
# #plot(pg.tout/year, Qbs[1,:])

# ylabel("Sediment ouput (m^3/s)")
# #legend([ "Mass conservation","Bedload at snout"])

subplot(3,1,3,sharex=ax)

plot(pg.tout/year, mean(hts,1)',      color = "b")
#ylim([0,0.3])
ylabel(L"Mean till thickness ($\mathrm{m}$)",fontsize=9)
legend(["whole glacier", "bottom cells"],fontsize=9)
axvline(pg.tout[check_pt]./year)

##################################

zs_=[]
zb_=[]
for j = 1:length(pg.sgrid)
    push!(zs_, zs(pg.sgrid[j],pg.tout[1],pg))
    push!(zb_, zb(pg.sgrid[j],pg.tout[1],pg))
end

source_out_H20=zeros(length(pg.sgrid))

for i = 1:length(source_out_H20)
    source_out_H20[i]=maximum(-Qws[i,:])
end

figure("profile at time $(pg.tout[check_pt]/year)")
ax = subplot(4,1,1)
plot(pg.sgrid, zs_, linewidth=1,color="blue");hold(true)
fill_between(pg.sgrid,zs_,0,color="skyblue")
plot(pg.sgrid, zb_, linewidth=1, color="black")
fill_between(pg.sgrid,zb_,color="black")
xlim([0,6000])
ylabel(L"Elevation ($\mathrm{m}$)")
# #axvline(x=pg.sgrid[cell]
tick_params(axis="both")

ax = subplot(4,1,2, sharex=ax)
plot(pg.sgrid, Qws[:,check_pt], linewidth=1,color="blue");hold(true)
xlim([0,6000])
ylabel("Qws")
#axvline(x=pg.sgrid[cell])
tick_params(axis="both")

ax = subplot(4,1,3, sharex=ax)
plot(pg.sgrid[1:end-1]+0.5*(pg.sgrid[1]-pg.sgrid[2]), hts[:,check_pt], linewidth=1)#, animated=true)#;hold(true)
xlim([0,6000])
ylabel(L"Till thickness ($\mathrm{m}$)")
#ylabel(L"dphi_dx",fontsize=20)
tick_params(axis="both")

subplot(4,1,4, sharex=ax)
plot(pg.sgrid, -Qbs[:,check_pt], linewidth=1, color = "red")#, animated=true)#;hold(true)
plot(pg.sgrid, -Qbes[:,check_pt], linewidth=1,color="blue");hold(true)
xlim([0,6000])
ylabel("Qbs")
legend(["actual", "equil."])
#ylabel(L"dphi_dx",fontsize=20)
tick_params(axis="both" )
