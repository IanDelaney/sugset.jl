# Bench-like synthetic run
using Parameters
using PyPlot
import Roots: find_zero
using SUGSET
using SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm_sm, source_water_ddm, Valley

############################
### parameter definition ###
############################

pg = Valley(tspan=(0,5*365*day),
            Q_sm = 2*day,
            fsl=1)

pp = Phys(DT=4,
          DAMP=16, # annual variation set to SHMIP
          DDAMP=2, #daily variation
          Dm=0.02)

pn = Num()

###############################
### shape  updated from SHMIP ###
###############################
"""
Part of delta H Huss et. al., 2010

dh_term(t,pg,pp)
equal to pg.B_0_term

TODO: set to pg.b_0

"""
dh_term(t,pg,pp) = pg.B_0_term # * div.(t,year) # mass balance at terminous of glacier... TODO


"""
Part of delta H Huss et. al., 2010

h_r_n(s,t,pg)

normalize elevation
"""
h_r_n(s,t,pg) = 1-((100*sqrt(sqrt((s+200))) + 1/60*s - 2e10^(1/4) + 1)/(100*sqrt(sqrt((pg.domain[2]+200))) + 1/60*pg.domain[2] - 2e10^(1/4) + 1))


"""
Part of delta H Huss et. al., 2010

delta_h(s,t,pg,pp)
equation for normalized delta_h given Huss et al. 2010

"""
function delta_h(s,t,pg)
    @unpack sgrid = pg

    if maximum(sgrid) > 20000  # a,b,c,gam long (>20km)
        dh_para = [-0.02, 0.12, 0, 6]
    elseif maximum(sgrid) > 5000 && maximum(sgrid) <= 20000  # a,b,c,gam medium (5-20km)
        dh_para = [-0.05, 0.19, 0.01, 4]
    elseif maximum(sgrid) <=5000        # a,b,c,gam small (<5 km)
        dh_para = [-0.30, 0.60, 0.09, 0.6]
    end

    out = ((h_r_n(s,t,pg)+dh_para[1])^dh_para[4] + dh_para[2]*(h_r_n(s,t,pg)+dh_para[1])+dh_para[3])*dh_term(t,pg,pp)

    return out

end
# zs_base(s, t, pg::Valley) = (100*sqrt(sqrt((s+200))) + 1/60*s - 2e10^(1/4) + 1) # original function

# function zb(s, t, pg::Valley)
#     sg = pg.sgrid[end]
#     (zs_base(sg,t,pg) - pg.para*sg)/sg^2 * s^2 + pg.para*s
# end

function zs(s, t, pg::Valley)
    return max(zs_base(s,t,pg)- delta_h(s,t,pg), SUGSET.proglacial)
end


################################
### source of water and till ###
################################
source_water_sm(s,t,pg::Valley,pp) = source_water_ddm_sm(s,t,pg,pp)
source_water(s,t,pg,pp) = source_water_ddm(s,t,pg,pp)



#############################
### run model and process ###
#############################

println("starting run_model()")
println(now())
@time sol  = run_model(pg, pp, pn)
println("run_model() finished")

println("Post processing (taking some time...):")
@time hts, dht_dts, Qws, Qws_sm, dphi_dx, Qbs, Qbes = post_proc(sol,pg, pp, pn)
println(" ...finished.")

SUGSET.mass_con_check(sol,pg, pp, pn)

include("example_plotter.jl")
