# Bench-like synthetic run

using Parameters
using PyPlot
using VAWTools
import Roots: find_zero
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley

############################
### parameter definition ###
############################

using DifferentialEquations
pn = Num(
    smooth_mobilization = 0.00 # smooth mobilization when going from transport to supply limited over this range of ht [m]
)


################################
### source of water and till ###
################################

#SUGSET.Q_w_sm(s, t, pg, pp) = (3 - 3/6e3*s)
#SUGSET.Q_w(s,t,pg,pp) = [SUGSET.Q_w_sm(s, t, pg, pp) * (1-cos(2pi*t/day));]

#############################
### run model and process ###
#############################

tend = 1.5

pg = Valley(tspan=(0.5*day, tend*day),
            tout=0.5*day:3600:tend*day,
            source_average_time = 4*day
            )

pp = Phys(DT= 15.0,
          DAMP= 0.0,
          DDAMP=2.0,
          xsec_min=.40,
          hookeangle= deg2rad(20)
          )

t= pg.tout[17]

#########################
### include hydro-mod ###
#########################



fig, axs = subplots(3,1, sharex=true)
sca(axs[1])

plot(pg.sgrid, SUGSET.Ψˣ.(pg.sgrid,Ref(t),Ref(pg),Ref(pp)),"+",label="SUGSET ")
for i in eachindex(pg.tout)
    t = pg.tout[i]    
    Qstar  =  abs.(SUGSET.Q_w_sm(pg.sgrid,t,pg, pp)) # true
    Q_inst =  abs.(SUGSET.Q_w(pg.sgrid,t,pg, pp)) # true
    Dhs = SUGSET.xsect(pg.sgrid,t,pg,pp,pn)[2]
    plot(pg.sgrid, SUGSET.Ψ.(Dhs, Q_inst, Ref(pp)),label="$(round(Q_inst[1],digits=2)) m³")
end
legend()

Φˣ=cumsum(SUGSET.Ψˣ.(pg.sgrid,Ref(t),Ref(pg),Ref(pp)) .*diff(pg.sgrid)[1]) .+ rw*g*SUGSET.zb.(pg.sgrid,Ref(t),Ref(pg)) 
sca(axs[2])
plot(pg.sgrid,Φˣ,"+",label="SUGSET ")

for i in eachindex(pg.tout)
    t = pg.tout[i]    
    Qstar  =  abs.(SUGSET.Q_w_sm(pg.sgrid,t,pg, pp)) # true
    Q_inst =  abs.(SUGSET.Q_w(pg.sgrid,t,pg, pp)) # true
    Dhs = SUGSET.xsect(pg.sgrid,t,pg,pp,pn)[2]    
    Φ=cumsum(SUGSET.Ψ.(Dhs, Q_inst, Ref(pp)) .*diff(pg.sgrid)[1]) .+ rw*g*SUGSET.zb.(pg.sgrid,Ref(t),Ref(pg)) 
    plot(pg.sgrid,Φ )
end


sca(axs[3])
axhline(1, ms=2)
for i in eachindex(pg.tout)
    t = pg.tout[i]    
    
    Qstar  =  abs.(SUGSET.Q_w_sm(pg.sgrid,t,pg, pp)) # true
    Q_inst =  abs.(SUGSET.Q_w(pg.sgrid,t,pg, pp)) # true
    Dhs = SUGSET.xsect(pg.sgrid,t,pg,pp,pn)[2]    
    Φ=cumsum(SUGSET.Ψ.(Dhs, Q_inst, Ref(pp)) .*diff(pg.sgrid)[1]) .+ rw*g*SUGSET.zb.(pg.sgrid,Ref(t),Ref(pg)) 

pᵢ=rw*g*SUGSET.thick.(pg.sgrid,Ref(t),Ref(pg))
    Φ_m=rw*g*SUGSET.zb.(pg.sgrid,Ref(t),Ref(pg))
    Φ₀=Φ_m + pᵢ
    P=Φ-Φ_m
    ff=P./pᵢ
    plot(pg.sgrid,ff )
    ylim([0  ,3])
end




# plot(pg.sgrid, SUGSET.dphidx.(pg.sgrid,t,pg,pp),label="SUGSET ")
# plot(pg.sgrid, Psi_shreve.(pg.sgrid, 1), "+", label="hydro-mod-- Shreve, ff=$(pg.float_frac)")

# legend()

# @show maximum(abs.(SUGSET.dphidx.(pg.sgrid,t,pg,pp) - Psi_shreve.(pg.sgrid, pg.float_frac)))


# now compare output of SUGSET vs hydro-mod.jl

# tau from the hydro-mod.jl
#tau_hm = tau.(velo, pp.fi)
#@test tau_hm[1:end-1] ≈ tau.(v.(Qstar.(pg.sgrid), Dhs, pp.hookeangle), pp.fi)[1:end-1] # note: tau_hm[end]==NaN



error()
###############
### compare ###
###############

fig, axs = subplots(3,1, sharex=true)

Qstar =  -SUGSET.Q_w_sm(pg.sgrid,t,pg, pp) # true
Q_inst =  -SUGSET.Q_w(pg.sgrid,t,pg, pp) # true

#discharge
sca(axs[1])
plot(pg.sgrid, Qstar, label="Qˣ (m^3/s)")
#plot(pg.sgrid, Qstar)
# plot(pg.sgrid, SUGSET.Q_w.(pg.sgrid,t,pg, pp), label="Q (m^3/s)")
plot(pg.sgrid, Q_inst,label="Q (m^3/s)")
legend()

# #xsect
# sca(axs[2])

Dhs = SUGSET.Dh.(SUGSET.Ψˣ.(pg.sgrid,Ref(t),Ref(pg),Ref(pp)), Qstar,Ref(pg),Ref(pp))

# vel
velo = abs.(SUGSET.Q_w_sm(pg.sgrid,t,pg, pp))./SUGSET.xsect(pg.sgrid,t,pg,pp,pn)[1]
sca(axs[2])
plot(pg.sgrid , velo, label="SUGSET")
plot(pg.sgrid, abs.(Qstar)./SUGSET.Dh2S.(Dhs, Ref(pp.hookeangle)), ":", label="hydro-mod -- default paras")
ylabel("v (m/s)")
legend()

sca(axs[3])
plot(pg.sgrid, SUGSET.shr_str_w(pg.sgrid, t, pg, pp, pn)[1], label="SUGSET")
plot(pg.sgrid, 1/8 .*pp.fi.*(Q_inst./VAWTools.max_smooth.(Ref(pp.xsec_min),SUGSET.Dh2S.(Dhs, Ref(pp.hookeangle)),Ref(pn.max_delta_xsect))).^2, ":", label="hydro-mod -- default paras")
ylabel("tau")
legend()
