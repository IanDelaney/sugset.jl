# a script to make test results.
# run with runtest.jl outputs in REP0

using JLD

###########################
### make test variables ###
###########################

### Geometry ###
zs_=[]
zb_=[]
for j = 1:length(pg.sgrid)
    push!(zs_, zs(pg.sgrid[j],pg.tout[1],pg))
    push!(zb_, zb(pg.sgrid[j],pg.tout[1],pg))
end

### Parameters ###


### Qws ###

Qws_test = [Qws[check_place,check_time[1]] Qws[check_place,check_time[2]] Qws[check_place,check_time[3]] Qws[check_place,check_time[4]]  Qws[check_place,check_time[5]]]

### Qws_sm ###

Qws_sm_test = [Qws_sm[check_place,check_time[1]] Qws_sm[check_place,check_time[2]] Qws_sm[check_place,check_time[3]] Qws_sm[check_place,check_time[4]] Qws_sm[check_place,check_time[5]]]

### Qbes ###
Qbes_test = [Qbes[check_place,check_time[1]] Qbes[check_place,check_time[2]] Qbes[check_place,check_time[3]] Qbes[check_place,check_time[4]] Qbes[check_place,check_time[5]]]

### hts ###
hts_test = [hts[check_place,check_time[1]] hts[check_place,check_time[2]] hts[check_place,check_time[3]] hts[check_place,check_time[4]] hts[check_place,check_time[5]]]

### Qbs ###
Qbs_test = [Qbs[check_place,check_time[1]] Qbs[check_place,check_time[2]] Qbs[check_place,check_time[3]] Qbs[check_place,check_time[4]] Qbs[check_place,check_time[5]]]

###########################
### save test variables ###
###########################

save("SedErosion_test_results.jld",
     "zs_test",    zs_test, #geometry
     "zb_test",    zb_test,
     "test_time",  run_time,
     "ht0",        pg.ht0,  #parameters
     "fsl",        pg.fsl,
     "Dm",         pp.Dm,
     "lr",         pp.lr,
     "fi",         pp.fi,
     "ΔT",         pp.ΔT,
     "DAMP",       pp.DAMP,
     "DDAMP",      pp.DDAMP,
     "Δσ", pn.Δσ,
     "Qws_test",   Qws_test, #outputs
     "Qws_sm_test",Qws_sm_test,
     "Qbes_test",  Qbes_test,
     "hts_test",   hts_test,
     "Qbs_test",   Qbs_test
     )
