using Base.Test

const rw = 1000
const ri = 900
const g = 9.81
const day = 86400
"""
To account for Hooke-channels

For a circle this is 1/pi^2, for semi-circle 4*pi^2/(pi+2)^4.
"""
hookefact(hookeangle) = ( 2*(hookeangle - sin(hookeangle)) /
                          (hookeangle + 2*sin(hookeangle/2))^2 )^2

@test hookefact(2pi) ≈ 1/pi^2
@test hookefact(pi) ≈ 4 * pi^2/(pi+2)^4

# "Hyd diameter from Darcy-Weisbach"
Dh(Ψ⁺, f, Q⁺, hookeangle) = (f * 8*rw*(Q⁺)^2/Ψ⁺ * hookefact(hookeangle) )^(1/5)

Dh(x, f, Q::Function, Psi::Function, ff, hookeangle) = Dh(Psi(x, ff), f, Q(x), hookeangle)



"Hyd dia of Hooke channel (Werder & Funk 2009)"
function S2Dh(S, hookeangle)
    4* sqrt(S*(hookeangle - sin(hookeangle))) /
         (sqrt(2) * (hookeangle + sqrt(2 - 2*cos(hookeangle))))
end

function Dh2S(Dh, hookeangle)
    (Dh/4)^2 * (sqrt(2) * (hookeangle + sqrt(2 - 2*cos(hookeangle))))^2 /
        (hookeangle - sin(hookeangle))
end
@test S2Dh(Dh2S( 4, pi/5), pi/5) ≈ 4

# Clarke 2003 eq 4
@test S2Dh(pi*3^2, 2pi)/4 ≈ 3/2 # circle
@test S2Dh(pi*3^2/2, pi)/4 ≈ pi*3/(2*(pi+2)) # semi-circle

# "Calculate wetted perimeter from area"
# function S2Pw(S, hookeangle)
#     r = sqrt(2S/ ( hookeangle- sin(hookeangle))) # hooke radius
#     return r * (hookeangle + 2* sin(hookeangle/2))
# end

# "Calculate area from wetted perimeter"
# function Pw2S(Pw, hookeangle)
#     error()
# end

# Relation between hyd-dia and hyd-radius:
# Rh = S/P
# Dh = 4S/P
# Dh = 4Rh
# @test S2Pw(pi*3^2/2, pi) ≈ (pi+2)*3 # semi-circle

"Flow velocity"
v(Q, Dh, hookeangle) = Q/Dh2S(Dh, hookeangle)

"Shear stress"
tau(v, f) = 1/8*f*rw*v^2

# SHMIP Bench glacier https://shmip.bitbucket.io/instructions.html#sec-2-2
const para_bench = 0.05
surface(x) = 100(x+200)^(1/4) + 1/60*x - 2e10^(1/4) + 1
surface_dx(x) = 1/4* 100(x+200)^(1/4 - 1) + 1/60
bed(x) = (surface(6e3) - para_bench*6e3)/6e3^2 * x^2 + para_bench*x
bed_dx(x) = (surface(6e3) - para_bench*6e3)/6e3^2 * 2x + para_bench

Psi_shreve(x, ff) = bed_dx(x) * rw * g + ff*(surface_dx(x)-bed_dx(x)) * ri * g

## The instantaneous Psi for fixed Dh:
Psi(Dh, Q, f, hookeangle) = 8rw*f*Q^2/Dh^5 * hookefact(hookeangle)

Qstar(x) = 3 - 3/6e3*x
Q_inst(x,t) = Qstar(x) * (1-cos(2pi*t/day)) # t in days


using PyPlot

x = 0:100:6e3
hookeangle = pi/5 # increases v
ff = .99 # effect depends on x
fric = 0.06 # decreases v
t=0.5*day

fig, axs = subplots(4,1, sharex=true)
#topo
sca(axs[1])
plot(x, surface.(x))
plot(x, bed.(x))
ylabel("elevation (m)")

#gradients topo
sca(axs[2])
plot(x, surface_dx.(x))
plot(x, bed_dx.(x))

#discharge
sca(axs[1])
plot(x, Qstar.(x), label="Q^* (m^3/s)")
plot(x, Q_inst.(x,0.5), label="Q (m^3/s)")
legend()

#xsect
sca(axs[2])
Dhs = Dh.(x, fric, Qstar, Psi_shreve, ff, hookeangle)
plot(x, Dh2S.(Dhs, hookeangle), label="S (m^2)")
legend()

#vel
sca(axs[3])
plot(x, v.(Qstar.(x), Dhs, hookeangle), label="modified paras"  )
plot(x, v.(Qstar.(x), Dh.(Psi_shreve.(x, 1), fric, Qstar.(x), hookeangle), hookeangle)  , ":", label="default paras")
ylabel("v (m/s)")
legend()

#Psi
sca(axs[4])
figure()
plot(x, 1/rw/g*Psi_shreve.(x, 1), "+", label="Shreve, ff=1")

for t=0:0.125:0.99
    plot(x, 1/rw/g*Psi.(Dhs, Q_inst.(x,t), ff, hookeangle), label="t=$t days")
end

ylabel("Ψ (m H2O/m)")
legend()


#Integrate Psi
phi_shreve = cumsum([0; Psi_shreve.(x, 1)[1:end-1]*step(x)])
phi = t -> cumsum([0; Psi.(Dhs, Q_inst.(x,t), fric, hookeangle)[1:end-1]*step(x)])

figure()
plot(x, phi_shreve/rw/g, "+", label="Shreve ff=1")
for t=0:0.1*day:0.99*day
    plot(x, phi(t)/rw/g, label="t=$t days")
end
legend()
ylabel("ϕ (m H2O)")
