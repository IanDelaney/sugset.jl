using Parameters
using Statistics
using Test
using JLD
using Dates
using SUGSET
using SUGSET: erosion, post_proc, solvers

import SUGSET: zb, zs, source_water, width, source_till, thick, day, source_water_ddm, Valley

using Pkg
pkg"activate ."

##########################
### Read in test cases ###
##########################
per_acc = 0.001 # percent accuracy compared to mean of value of test parameter
#per_acc = 0.01 # percent accuracy compared to mean of value of test parameter


pg = Valley(tspan=(0, 0.8*365*day),
            domain=(0.0, 6000.0),
            sgrid=range(0.0,stop=6000.0,length=100)
            )

pg = Valley(pg,
            tout= pg.tspan[1]:0.25*day:pg.tspan[2]
            )

pp = Phys(ΔT=2.0,
          DAMP=16.0, # annual variation set to SHMIP
          DDAMP=1.0, #daily variation
          Dm=0.02)

pn = Num()

### test that objective function is type-stable
@inferred SUGSET.dht_dt_fn(pg.ht0, pg.sgrid, 0.0, pg, pp, pn)

#####################
### run the model ###
#####################

println("starting run_model()")
println(now())
sol, run_time, _, _, _ = @timed run_model(pg, pp, pn)

println("run_model() finished")

hts, dht_dts, sed_source, Qws, Qws_sm, Qbs, Qbes  = post_proc(sol,pg,pp,pn)

################################
### parse out data from run  ###
################################

t1 = 250      # year 0.204 Winter scenerio
t2 = 420      # year 0.34 melt onset scenerio
t3 = 475      # year 0.389 peak scenerio
t4 = 608      # year 0.498 middle of melt season
t5 = 775      # year 0.63 just before stuff slows down

check_time = [t1 t2 t3 t4 t5]

spot1 = 1
spot2 = 15
spot3 = 30
spot4 = 60
spot5 = 95

check_place =[spot1 spot2 spot3 spot4 spot5]

### topo ###

test_cases=load("test/SedErosion_test_results.jld")


zs_test     = test_cases["zs_test"]
zb_test     = test_cases["zb_test"]
test_time   = test_cases["test_time"]
ht0_test    = test_cases["ht0"]
fsl_test    = test_cases["fsl"]
Dm_test     = test_cases["Dm"]
lr_test     = test_cases["lr"]
fi_test     = test_cases["fi"]
ΔT_test     = test_cases["ΔT"]
DAMP_test   = test_cases["DAMP"]
DDAMP_test  = test_cases["DDAMP"]
Δσ_test = test_cases["Δσ"]
Qws_test    = test_cases["Qws_test"]
Qws_sm_test = test_cases["Qws_sm_test"]
Qbes_test   = test_cases["Qbes_test"]
hts_test    = test_cases["hts_test"]
Qbs_test    = test_cases["Qbs_test"]


zs_=[]
zb_=[]
for j = 1:length(pg.sgrid)
    push!(zs_, zs(pg.sgrid[j],pg.tout[1],pg))
    push!(zb_, zb(pg.sgrid[j],pg.tout[1],pg))
end

### water ###
Qws_runner = [Qws[check_place,check_time[1]] Qws[check_place,check_time[2]] Qws[check_place,check_time[3]] Qws[check_place,check_time[4]]  Qws[check_place,check_time[5]]]
Qws_sm_runner = [Qws_sm[check_place,check_time[1]] Qws_sm[check_place,check_time[2]] Qws_sm[check_place,check_time[3]] Qws_sm[check_place,check_time[4]] Qws_sm[check_place,check_time[5]]]

### Qbes ###
Qbes_runner = [Qbes[check_place,check_time[1]] Qbes[check_place,check_time[2]] Qbes[check_place,check_time[3]] Qbes[check_place,check_time[4]] Qbes[check_place,check_time[5]]]

### hts ###
hts_runner = [hts[check_place,check_time[1]] hts[check_place,check_time[2]] hts[check_place,check_time[3]] hts[check_place,check_time[4]] hts[check_place,check_time[5]]]

### Qbs ###
Qbs_runner = [Qbs[check_place,check_time[1]] Qbs[check_place,check_time[2]] Qbs[check_place,check_time[3]] Qbs[check_place,check_time[4]] Qbs[check_place,check_time[5]]]

################
### Testing  ###
################

topo_test = [zs_test zb_test]
topo_runner = [zs_ zb_]

mass_con =  SUGSET.mass_con_check(sol, pg, pp, pn)

@test mass_con[1] < 0.01 # make sure it is less then 1 %

#@test topo_test ≈ topo_runner atol=0.1

### parameters ###

@test pg.ht0 ≈  ht0_test atol=0.001
@test pg.fsl ≈  fsl_test atol=0.001
@test pp.Dm ≈  Dm_test atol=0.001
@test pp.fi ≈ fi_test atol=0.001
@test pp.lr ≈  lr_test atol=0.0001
@test pp.ΔT ≈  ΔT_test atol=0.0001
@test pp.DAMP ≈  DAMP_test atol=0.0001
@test pn.Δσ ≈Δσ_test atol = 0.000001
@test pp.DDAMP ≈  DDAMP_test atol=0.0001

### non-solver dependent test ###

water_test   = [Qws_test   Qws_sm_test]
water_runner = [Qws_runner Qws_sm_runner]

#@test water_test ≈ water_runner atol=0.1

@test Qbes_test ≈ Qbes_runner atol=per_acc*mean(abs.(Qbes_test))

### objfunc dependent test ###

@test hts_test ≈ hts_runner atol=per_acc*mean(hts_test) # 1 mm

### post_proc test ###

@test Qbs_test ≈ Qbs_runner atol=per_acc*mean(abs.(Qbs_test))

println("\n !!! time test!!! \n
         this run: $(run_time)\n
         test run: $(test_time)")
