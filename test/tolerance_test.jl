# ISM-like synthetic run
using Test
using Parameters, PyPlot
using SUGSET
using SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, source_water, width, source_till, thick, Valley, source_water_ddm, find_sp, year, day, tol_tester

debugplot = false

experiment = ""

grid_num = 1 .+ [3200, 1600, 800, 400, 200, 100]
#grid_num = 1 .+ [ 400, 200, 100]

#grid_num = 1 .+ [ 800, 400, 200, 100]

tol = [1e-9,  1e-8, 1e-7, 1e-6, 1e-5, 1e-4]
#$tol = [1e-6, 1e-5, 1e-4]

############
### data ###
############
pp = Phys()

pg= Valley(tspan = (0,year))

pn = Num()

#error_array, error_array_rel, ΔV_array, pg_ref =  tol_tester(Valley, grid_num, tol, pg, pp, pn, experiment, 0.4)

################
### plotting ###
################
fig, axs = subplots(1,2,figsize=(8,5))
sca(axs[1])
# err vs tol
for i=1:length(grid_num)
    sgrids = step(range(pg_ref.domain[1],stop=pg_ref.domain[2],length=grid_num[i]))
    loglog(tol, error_array[i,:],linewidth="1", marker="o",label ="$sgrids m" )
    ylabel(L"abs. error of H (m a$^{-1}$)",fontsize=12)
    xlabel("tolerance (m)", fontsize=12)
end
ylim([10^-5, 7^-3])
text(1e-9, 8^-3, "a", fontsize=20)
legend()

# err vs grid-size
sca(axs[2])
sgrids = ones(length(grid_num))
for i = 1:length(grid_num)
    sgrids[i] = step(range(pg_ref.domain[1],stop=pg_ref.domain[2],length=grid_num[i]))
end

for i=1:length(tol)

    loglog(sgrids, error_array[:,i],linewidth="1", marker="o",label ="$(tol[i])" )
   # ylabel("absolute error of H (m)",fontsize=12)
    xlabel("grid spacing (m)", fontsize=12)
end
text(1.2, 8^-3, "b", fontsize=20)
ylim([10^-5, 7^-3])
xlim([1.1 , 100])
legend()

# sca(axs[2])
# # err vs tol
# for i=1:length(grid_num)
#     sgrids = step(linspace(pg_ref.domain[1],pg_ref.domain[2],grid_num[i]))
#     semilogx(tol, ΔV_array[i,:],linewidth="1", marker="o",label ="$sgrids m" ); hold(true)
#     ylabel(L"norm Q$_{s}$",fontsize=12)
#     xlabel("tolerance (m)", fontsize=12)
# end
# #text(1e-8, 25e-5, "a", fontsize=18)
# legend()

# sca(axs[4])
# sgrids = step.(linspace.(pg_ref.domain[1],pg_ref.domain[2],grid_num))
# for i=1:length(tol)
#     plot(sgrids, ΔV_array[:,i],linewidth="1", marker="o",label ="$(tol[i])" ); hold(true)
#    # ylabel("absolute error of H (m)",fontsize=12)
#     xlabel("grid spacing (m)", fontsize=12)
# end
# #xlim([1.1 , 100])
# legend()





#legend(["$(round(grid_num[1]./pg.sgrid[end],3)) m" ,  "$(round(grid_num[2]./pg.sgrid[end],3)) m", "$(round(grid_num[3]./pg.sgrid[end],3) m", "$(round(grid_num[4]./pg.sgrid[end],3)) m" , "$(round(grid_num[5]./pg.sgrid[end],3)) m")
savefig("/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/tol_err.pdf")
